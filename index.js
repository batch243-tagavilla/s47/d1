// console.log("Hello word");

// [Section] Document Object Model (DOM)
/* 
    - allows us to access or modify the properties of an HTML element in a webpage
    - it is a standard on how to get, change, add, or delete HTML elements
    - we will focus on use of DOM in managing forms
*/

// For selectiong HTML elements, we will be using document.querySelector
/* 
    Syntax:
    - document.querySelector("html element")
    - the querySelector function takes a string input that is formated like a CSS selector when applying the styles
*/

/* const txtFirstName = document.querySelector("#txt-first-name"); */
/* console.log(txtFirstName); */

const name = document.querySelectorAll(".full-name");
/* console.log(name); */

const span = document.querySelectorAll("span");
/* console.log(span); */

const text = document.querySelectorAll("input[type]");
/* console.log(text); */

const title = document.querySelectorAll("title");
/* console.log(title); */

// [Section] Event Listeners
/* 
    - whenever a user interqqacts with a webpage, this action is considered as an event
    - working with events is a large part of creating interactivity in a webpage
    - specific functions that will perform an action
*/

// "addEventListener"
// first argument - a string identifying the event
// second argument - function that the listener will trigger once the "specified event" is triggered

/* txtFirstName.addEventListener("change", (event) => {
    console.log(event.target.value);
}); */

/* ACTIVITY */
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#fullName");
let color = document.querySelector("#colorPicker").value;

const fullName = () => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
    spanFullName.style.color = color
};

const changeColor = () => {
    color = document.querySelector("#colorPicker").value;
    spanFullName.style.color = color
};

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);
colorPicker.addEventListener("change", changeColor);
